package md.gg.ancientrivals.mapeditor.utils

import md.gg.ancientrivals.mapeditor.data.domain.MapNode

fun convertConnectionToNodes(connections: List<Pair<MapNode, MapNode>>): MutableList<MapNode> {
    val result: MutableList<MapNode> = mutableListOf()
    connections.forEach {
        if (!result.contains(it.first)) result.add(it.first)
        if (!result.contains(it.second)) result.add(it.second)
    }
    return result
}

fun uniqueConnections(connections: List<Pair<MapNode, MapNode>>): MutableList<Pair<MapNode, MapNode>> {
    val result: MutableList<Pair<MapNode, MapNode>> = mutableListOf()
    connections.forEach {
        if (!result.contains(it)) result.add(it)
    }
    return result
}
