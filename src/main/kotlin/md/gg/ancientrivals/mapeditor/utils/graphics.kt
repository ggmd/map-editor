package md.gg.ancientrivals.mapeditor.utils

import md.gg.ancientrivals.mapeditor.data.domain.MapNode

fun centerPoint(nodes: List<MapNode>): Pair<Double, Double> {
    var totalX = 0.0
    var totalY = 0.0
    nodes.forEach {
        totalX += it.x
        totalY += it.y
    }
    return Pair(totalX / nodes.size, totalY / nodes.size)
}