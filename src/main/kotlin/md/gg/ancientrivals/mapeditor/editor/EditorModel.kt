package md.gg.ancientrivals.mapeditor.editor

import md.gg.ancientrivals.mapeditor.app.MainModel
import md.gg.ancientrivals.mapeditor.data.MapRepository
import md.gg.ancientrivals.mapeditor.data.domain.MapNode
import md.gg.ancientrivals.mapeditor.data.domain.Province
import tornadofx.*

class EditorModel : Component(), Injectable {
    var mapNodes: MutableList<MapNode> = mutableListOf()
    var mapConnections: MutableList<Pair<MapNode, MapNode>> = mutableListOf()

    private val mapRepository: MapRepository by inject()
    private val mainModel: MainModel by inject()

    val selectedNodes: MutableList<MapNode>
        get() = mainModel.selectedNodes

    val provinces: MutableList<Province>
        get() = mapRepository.provinces

    var mapOffset: Pair<Double, Double>
        get() = mapRepository.map.offset
        set(value) {
            mapRepository.map.offset = value
        }


    var selectedProvince: Province?
        get() = mainModel.selectedProvince
        set(value) {
            mainModel.selectedProvince = value
        }

    fun addMapNode(mapNode: MapNode) {
        mapNodes.add(mapNode)
    }

    fun addMapConnection(mapNodeFrom: MapNode, mapNodeTo: MapNode) {
        mapConnections.add(Pair(mapNodeFrom, mapNodeTo))
    }

    fun removeNode(mapNode: MapNode): Boolean {
        mapNodes.remove(mapNode)
        mapConnections.removeIf({ it.first == mapNode || it.second == mapNode })
        return mapRepository.provinces.removeIf {
            it.mapConnections.any {
                it.first == mapNode || it.second == mapNode
            }
        }
    }

    fun getClosestSelectedMapNode(x: Double, y: Double): MapNode {
        var leastDistance = Double.MAX_VALUE
        var closestNode = MapNode()
        for (mapNode in mainModel.selectedNodes) {
            val distance = measureDistance(x, y, mapNode.x, mapNode.y)
            if (distance < leastDistance) {
                leastDistance = distance
                closestNode = mapNode
            }
        }
        return closestNode
    }

    fun getClosestMapNode(x: Double, y: Double): MapNode {
        var leastDistance = Double.MAX_VALUE
        var closestNode = MapNode()
        for (mapNode in mapNodes) {
            val distance = measureDistance(x, y, mapNode.x, mapNode.y)
            if (distance < leastDistance) {
                leastDistance = distance
                closestNode = mapNode
            }
        }
        return closestNode
    }

    fun getSecondClosestMapNode(mapNodeToIgnore: MapNode, x: Double, y: Double): MapNode {
        var leastDistance = Double.MAX_VALUE
        var closestNode = MapNode()
        for (mapNode in mapNodes) {
            if (mapNode == mapNodeToIgnore) {
                continue
            }
            val distance = measureDistance(x, y, mapNode.x, mapNode.y)
            if (distance < leastDistance) {
                leastDistance = distance
                closestNode = mapNode
            }
        }
        return closestNode
    }

    private fun measureDistance(originX: Double, originY: Double, destX: Double, destY: Double): Double {
        val deltaX = Math.abs(originX - destX)
        val deltaY = Math.abs(originY - destY)
        return Math.sqrt(deltaX * deltaX + deltaY * deltaY)
    }
}
