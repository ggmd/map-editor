package md.gg.ancientrivals.mapeditor.editor

import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import md.gg.ancientrivals.mapeditor.data.domain.MapNode
import md.gg.ancientrivals.mapeditor.province_inspector.ProvinceInspectorView
import md.gg.ancientrivals.mapeditor.province_list.ProvinceListView
import md.gg.ancientrivals.mapeditor.utils.convertConnectionToNodes
import md.gg.ancientrivals.mapeditor.utils.uniqueConnections
import tornadofx.*

class EditorPresenter : Controller() {
    companion object {
        val EDITOR_ZOOM_DELTA = 0.1
    }

    private val view: EditorView by inject()
    private val provinceListView: ProvinceListView by inject()
    private val provinceInspectorView: ProvinceInspectorView by inject()
    private val model: EditorModel by inject()

    var holdingShift: Boolean = false

    var closestNode: MapNode? = null

    fun init() {
        val allConnections: MutableList<Pair<MapNode, MapNode>> = mutableListOf()
        model.provinces.forEach {
            allConnections.addAll(it.mapConnections)
        }
        val uniqueConnections = uniqueConnections(allConnections)
        model.mapNodes = convertConnectionToNodes(uniqueConnections)
        model.mapConnections = uniqueConnections

        subscribeToCanvasMouseMoved()
        subscribeToCanvasMouseClicked()
        subscribeToCanvasMouseDragged()
        subscribeToCanvasDragDetected()

        subscribeToCanvasScroll()

        subscribeToCanvasClear()

        model.mapOffset = Pair(view.canvas.width / 2, view.canvas.height / 2)
    }

    private fun subscribeToCanvasDragDetected() {
        view.canvasMouseDragDetectedSubject.subscribe {
            if (it.button != MouseButton.MIDDLE) {
                it.consume()
            }
        }
    }

    private fun subscribeToCanvasClear() {
        view.canvasClearSubject.subscribe {
            view.drawNodes(model.mapNodes)
        }
        view.canvasClearSubject.subscribe {
            view.drawConnections(model.mapConnections)
        }
        view.canvasClearSubject.subscribe {
            view.drawSelection(model.selectedNodes, model.mapConnections)
        }
    }

    private fun subscribeToCanvasScroll() {
        view.canvasScrollSubject.subscribe {
            if (it.deltaY > 0) {
                view.zoomIn(EDITOR_ZOOM_DELTA)
            } else {
                view.zoomOut(EDITOR_ZOOM_DELTA)
            }
            it.consume()
        }
    }

    private fun subscribeToCanvasMouseDragged() {
        view.canvasMouseDraggedSubject.subscribe {
            if (it.button != MouseButton.MIDDLE) it.consume()
        }
    }

    private fun subscribeToCanvasMouseMoved() {
        view.canvasMouseMovedSubject.subscribe {
            if (it.button == MouseButton.NONE) {
                onMouseDraggedNone(it)
            }
        }
    }

    private fun subscribeToCanvasMouseClicked() {
        view.canvasMouseClickedSubject.subscribe {
            if (it.button == MouseButton.PRIMARY) {
                onMousePrimary(it)
            }
            if (it.button == MouseButton.SECONDARY) {
                onMouseSecondary(it)
            }
        }
    }

    private fun onMousePrimary(it: MouseEvent) {
        if (it.isControlDown) {
            onMousePrimaryControlDown(it)
        } else if (it.isShiftDown) {
            onMousePrimaryShiftDown(it)
        } else if (it.isAltDown) {
            onMousePrimaryAltDown(it)
        }
    }

    private fun onMousePrimaryShiftDown(it: MouseEvent) {
        val secondClosestNode = model.getSecondClosestMapNode(closestNode!!, it.x, it.y)
        model.addMapConnection(closestNode!!, secondClosestNode)

        view.updateCanvas(model.provinces, model.selectedProvince)
    }

    private fun onMousePrimaryControlDown(it: MouseEvent) {
        model.addMapNode(MapNode(it.x, it.y))
        view.updateCanvas(model.provinces, model.selectedProvince)
    }

    private fun onMousePrimaryAltDown(it: MouseEvent) {
        closestNode = null
        if (model.removeNode(model.getClosestMapNode(it.x, it.y))) {
            provinceListView.showProvinces(model.provinces)
            provinceInspectorView.clear()
        }
        view.updateCanvas(model.provinces, model.selectedProvince)
    }

    private fun onMouseSecondary(it: MouseEvent) {
        if (it.isControlDown) {
            onMouseSecondaryControlDown(it)
        } else {
            model.selectedNodes.remove(model.getClosestSelectedMapNode(it.x, it.y))
        }
        view.updateCanvas(model.provinces, model.selectedProvince)
    }

    private fun onMouseSecondaryControlDown(event: MouseEvent) {
        val newSelected = model.getClosestMapNode(event.x, event.y)
        if (!model.selectedNodes.contains(newSelected)) {
            model.selectedNodes.add(newSelected)
        }
    }

    private fun onMouseDraggedNone(it: MouseEvent) {
        if (it.isControlDown) {
            onMouseNoneControlDown(it)
        } else if (it.isShiftDown) {
            onMouseNoneShiftDown(it)
        } else {
            onMouseNoneNothingDown()
        }
    }

    private fun onMouseNoneNothingDown() {
        holdingShift = false
        view.updateCanvas(model.provinces, model.selectedProvince)
    }

    private fun onMouseNoneShiftDown(it: MouseEvent) {
        view.updateCanvas(model.provinces, model.selectedProvince)
        if (holdingShift) {
            view.drawLineGhost(it.x, it.y, closestNode!!.x, closestNode!!.y)
        } else {
            closestNode = model.getClosestMapNode(it.x, it.y)
            view.drawLineGhost(it.x, it.y, closestNode!!.x, closestNode!!.y)
        }
        holdingShift = true
    }

    private fun onMouseNoneControlDown(it: MouseEvent) {
        view.updateCanvas(model.provinces, model.selectedProvince)
        view.drawNodeGhost(it.x, it.y)
    }
}