package md.gg.ancientrivals.mapeditor.editor

import io.reactivex.subjects.BehaviorSubject
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.geometry.VPos
import javafx.scene.canvas.Canvas
import javafx.scene.control.ScrollPane
import javafx.scene.effect.DropShadow
import javafx.scene.input.MouseEvent
import javafx.scene.input.ScrollEvent
import javafx.scene.paint.Color
import javafx.scene.text.TextAlignment
import md.gg.ancientrivals.mapeditor.data.domain.MapNode
import md.gg.ancientrivals.mapeditor.data.domain.Province
import md.gg.ancientrivals.mapeditor.utils.centerPoint
import md.gg.ancientrivals.mapeditor.utils.convertConnectionToNodes
import tornadofx.*

class EditorView : View() {
    companion object {
        const val SHADOW_OFFSET = 2.0

        const val GRID_WIDTH = 200
        const val GRID_HEIGHT = 100
        const val GRID_CELL_SIZE = 10.0
        const val GRID_LINE_THICKNESS = 0.1

        const val LINE_WIDTH = 1.0
        const val SELECTION_WIDTH = 1

        const val CANVAS_WIDTH = 1000.0
        const val CANVAS_HEIGHT = 1000.0
    }

    val canvasMouseMovedSubject: BehaviorSubject<MouseEvent> = BehaviorSubject.create()
    val canvasMouseClickedSubject: BehaviorSubject<MouseEvent> = BehaviorSubject.create()
    val canvasMouseDraggedSubject: BehaviorSubject<MouseEvent> = BehaviorSubject.create()
    val canvasMouseDragDetectedSubject: BehaviorSubject<MouseEvent> = BehaviorSubject.create()
    val canvasScrollSubject: BehaviorSubject<ScrollEvent> = BehaviorSubject.create()
    val canvasClearSubject: BehaviorSubject<Any> = BehaviorSubject.create()

    val canvas = Canvas().apply {
        height = CANVAS_WIDTH
        width = CANVAS_HEIGHT
    }

    override val root = scrollpane {
        fitToHeightProperty().set(true)
        fitToWidthProperty().set(true)
        pannableProperty().set(true)
        hbarPolicy = ScrollPane.ScrollBarPolicy.ALWAYS
        vbarPolicy = ScrollPane.ScrollBarPolicy.ALWAYS
        content = vbox {
            alignment = Pos.CENTER
            hbox {
                alignment = Pos.CENTER
                vbox {
                    children.add(canvas)
                }
            }
        }
    }

    init {
        root.hvalue = 0.5
        root.vvalue = 0.5

        overrideEditorEvents()

        setupGrid()
        setupAxis()

        addShadowToScrollPane()
    }

    fun drawSelection(selectedNodes: List<MapNode>, connections: List<Pair<MapNode, MapNode>>) {
        val gc = canvas.graphicsContext2D
        gc.stroke = Color.DARKORANGE
        gc.lineWidth = LINE_WIDTH

        for ((x, y) in selectedNodes) {
            gc.strokeOval(x, y, GRID_CELL_SIZE, GRID_CELL_SIZE)
        }

        val selectedEdges = connections.filter { selectedNodes.contains(it.first) and selectedNodes.contains(it.second) }
        for ((f, s) in selectedEdges) {
            gc.strokeLine(f.x, f.y, s.x, s.y)
        }
    }

    fun zoomIn(delta: Double) {
        canvas.scaleX += delta
        canvas.scaleY += delta
    }

    fun zoomOut(delta: Double) {
        canvas.scaleX -= delta
        canvas.scaleY -= delta
    }

    fun drawNodeGhost(x: Double, y: Double) {
        val gc = canvas.graphicsContext2D
        gc.fill = Color.LIGHTGRAY
        gc.stroke = Color.LIGHTBLUE
        gc.fillOval(x, y, GRID_CELL_SIZE, GRID_CELL_SIZE)
    }

    fun drawNodes(mapNodes: List<MapNode>) {
        val gc = canvas.graphicsContext2D
        gc.fill = Color.BLACK
        gc.stroke = Color.LIGHTGRAY
        for ((x, y) in mapNodes) {
            gc.fillOval(x, y, GRID_CELL_SIZE, GRID_CELL_SIZE)
        }
    }

    fun drawConnections(mapConnections: List<Pair<MapNode, MapNode>>) {
        val gc = canvas.graphicsContext2D
        gc.stroke = Color.BLACK
        gc.lineWidth = LINE_WIDTH
        for ((first, second) in mapConnections) {
            gc.strokeLine(
                    first.x + GRID_CELL_SIZE / 2, first.y + GRID_CELL_SIZE / 2,
                    second.x + GRID_CELL_SIZE / 2, second.y + GRID_CELL_SIZE / 2)
        }
    }

    fun drawLineGhost(fromX: Double, fromY: Double, toX: Double, toY: Double) {
        val gc = canvas.graphicsContext2D
        gc.stroke = Color.LIGHTGREY
        gc.lineWidth = LINE_WIDTH
        gc.strokeLine(fromX, fromY, toX + GRID_CELL_SIZE / 2, toY + GRID_CELL_SIZE / 2)
    }

    fun updateCanvas(provinces: List<Province>, selected: Province?) {
        val gc = canvas.graphicsContext2D
        gc.clearRect(0.0, 0.0, canvas.width, canvas.height)
        setupGrid()
        setupAxis()
        drawProvincesNames(provinces)
        if (selected != null) {
            drawProvinceSelection(selected)
        }
        canvasClearSubject.onNext(Any())
    }

    private fun drawProvinceSelection(province: Province) {
        val gc = canvas.graphicsContext2D
        gc.stroke = Color.DARKRED
        gc.lineWidth = LINE_WIDTH * 1.5
        for ((f, s) in province.mapConnections) {
            gc.strokeLine(f.x, f.y, s.x, s.y)
        }
    }

    private fun overrideEditorEvents() {
        canvas.addEventHandler(MouseEvent.MOUSE_MOVED, { canvasMouseMovedSubject.onNext(it) })
        canvas.addEventHandler(MouseEvent.MOUSE_CLICKED, { canvasMouseClickedSubject.onNext(it) })
        canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, { canvasMouseDraggedSubject.onNext(it) })
        canvas.addEventHandler(MouseEvent.DRAG_DETECTED, { canvasMouseDragDetectedSubject.onNext(it) })

        canvas.onScroll = EventHandler { canvasScrollSubject.onNext(it) }
    }

    private fun addShadowToScrollPane() {
        val dropShadow: DropShadow = DropShadow()
        dropShadow.offsetY = SHADOW_OFFSET
        dropShadow.offsetY = SHADOW_OFFSET
        dropShadow.radius = SHADOW_OFFSET
        root.effect = dropShadow
    }

    private fun drawProvincesNames(provinces: List<Province>) {
        val gc = canvas.graphicsContext2D
        gc.fill = Color.BLACK
        gc.textAlign = TextAlignment.CENTER
        gc.textBaseline = VPos.CENTER
        provinces.forEach {
            val nodes = convertConnectionToNodes(it.mapConnections)
            val center = centerPoint(nodes)
            gc.fillText(it.name, center.first, center.second)
        }
    }

    private fun setupGrid() {
        val gc = canvas.graphicsContext2D
        gc.fill = Color.LIGHTGREY
        gc.stroke = Color.LIGHTGREY
        gc.lineWidth = GRID_LINE_THICKNESS
        canvas.width = GRID_WIDTH * GRID_CELL_SIZE
        canvas.height = GRID_HEIGHT * GRID_CELL_SIZE

        for (i in 0..GRID_WIDTH) {
            gc.strokeLine(i * GRID_CELL_SIZE, 0.0, i * GRID_CELL_SIZE, canvas.height)
        }

        for (i in 0..GRID_HEIGHT) {
            gc.strokeLine(0.0, i * GRID_CELL_SIZE, canvas.width, i * GRID_CELL_SIZE)
        }
    }

    private fun setupAxis() {
        val gc = canvas.graphicsContext2D
        gc.lineWidth = 0.5

        gc.stroke = Color.LIGHTSALMON
        gc.strokeLine(0.0, canvas.height / 2, canvas.width, canvas.height / 2)

        gc.stroke = Color.LIGHTGREEN
        gc.strokeLine(canvas.width / 2, 0.0, canvas.width / 2, canvas.height)
    }
}