package md.gg.ancientrivals.mapeditor.data.domain

data class GameMap(
        var provinces: MutableList<Province>,
        var offset: Pair<Double, Double> = Pair(0.0, 0.0)
)