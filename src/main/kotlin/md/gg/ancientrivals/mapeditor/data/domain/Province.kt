package md.gg.ancientrivals.mapeditor.data.domain

import java.io.Serializable

data class Province(
        var name: String = "",
        var capital: Civilization = Civilization.NONE,
        var type: ProvinceType = ProvinceType.TERRAIN,
        var cities: MutableList<City> = mutableListOf(),
        var resources: MutableList<Resource> = mutableListOf(),
        var mapConnections: List<Pair<MapNode, MapNode>> = listOf()
) : Serializable