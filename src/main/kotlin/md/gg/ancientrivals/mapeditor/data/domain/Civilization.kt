package md.gg.ancientrivals.mapeditor.data.domain

import java.io.Serializable

enum class Civilization(var capital: String) : Serializable {
    NONE("None"),
    GREECE("Greece"),
    ROME("Rome"),
    EGYPT("Egypt"),
    BABYLON("Babylon"),
    CARTHAGE("Carthage")
}