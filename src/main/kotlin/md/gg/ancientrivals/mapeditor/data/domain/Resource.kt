package md.gg.ancientrivals.mapeditor.data.domain

import java.io.Serializable

enum class Resource(var resourceName: String) : Serializable {
    SLAVES("Slaves"),
    GRAIN("Grain"),
    GOLD("Gold"),
    IRON("Iron"),
    FISH("Fish"),
    CATTLE("Cattle"),
    WINE("Wine"),
    FRUITS("Fruits"),
    OIL("Oil"),
    GEMS("Gems"),
    PAPYRUS("Papyrus"),
    INCENSE("Incense")
}