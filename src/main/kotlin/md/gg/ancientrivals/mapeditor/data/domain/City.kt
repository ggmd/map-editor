package md.gg.ancientrivals.mapeditor.data.domain

import java.io.Serializable

data class City(var name: String = "") : Serializable