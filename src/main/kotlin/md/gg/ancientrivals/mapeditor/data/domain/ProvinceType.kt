package md.gg.ancientrivals.mapeditor.data.domain

import java.io.Serializable

enum class ProvinceType(var type: String) : Serializable {
    TERRAIN("Terrain"), SEA("Sea")
}