package md.gg.ancientrivals.mapeditor.data

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import md.gg.ancientrivals.mapeditor.data.domain.GameMap
import md.gg.ancientrivals.mapeditor.data.domain.Province
import tornadofx.*
import java.io.File
import java.io.FileNotFoundException

class MapRepository : Component(), Injectable {
    companion object {
        const val FILE_NAME = "./map.json"
    }

    var map = GameMap(mutableListOf())
    var provinces: MutableList<Province>
        get() = map.provinces
        set(value) {
            map.provinces = value
        }

    private val mapper: ObjectMapper = ObjectMapper().registerModule(KotlinModule())

    init {
        try {
            load()
        } catch (e: FileNotFoundException) {
            System.err.println("no map file: " + e.message)
        }
    }

    private fun load() {
        map = mapper.readValue(File(FILE_NAME))
    }

    fun save() {
        val fileOut = File(FILE_NAME)
        mapper.writeValue(fileOut, map)
    }
}