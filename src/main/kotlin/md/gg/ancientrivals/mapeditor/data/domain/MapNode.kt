package md.gg.ancientrivals.mapeditor.data.domain

import java.io.Serializable

data class MapNode(var x: Double = 0.0, var y: Double = 0.0) : Serializable