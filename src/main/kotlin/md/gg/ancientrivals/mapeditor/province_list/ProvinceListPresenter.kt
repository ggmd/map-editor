package md.gg.ancientrivals.mapeditor.province_list

import tornadofx.Controller

class ProvinceListPresenter : Controller() {
    private val view: ProvinceListView by inject()
    private val model: ProvinceListModel by inject()

    fun init() {
        subscribeToProvinceToggle()

        view.showProvinces(model.provinces)

        subscribeToWindowResize()
    }

    private fun subscribeToProvinceToggle() {
        view.provinceToggleSubject.subscribe {
            model.selectedProvince = it
            view.inspectProvince(it)
        }
    }

    private fun subscribeToWindowResize() {
        view.windowResize.subscribe {
            view.alignProvinciesList()
        }
    }
}