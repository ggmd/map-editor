package md.gg.ancientrivals.mapeditor.province_list

import md.gg.ancientrivals.mapeditor.app.MainModel
import md.gg.ancientrivals.mapeditor.data.MapRepository
import md.gg.ancientrivals.mapeditor.data.domain.Province
import tornadofx.*

class ProvinceListModel : Component(), Injectable {
    private val mapRepository: MapRepository by inject()
    private val mainModel: MainModel by inject()

    val provinces: List<Province>
        get() = mapRepository.provinces

    var selectedProvince: Province?
        get() = mainModel.selectedProvince
        set(value) {
            mainModel.selectedProvince = value
        }
}