package md.gg.ancientrivals.mapeditor.province_list

import com.jfoenix.controls.JFXRadioButton
import io.reactivex.subjects.BehaviorSubject
import javafx.geometry.Insets
import javafx.scene.control.ScrollPane
import javafx.scene.control.ToggleGroup
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import md.gg.ancientrivals.mapeditor.app.MainView
import md.gg.ancientrivals.mapeditor.data.domain.Province
import tornadofx.View
import tornadofx.addChildIfPossible
import tornadofx.flowpane
import tornadofx.hgrow
import tornadofx.scrollpane
import tornadofx.vgrow

class ProvinceListView : View() {
    lateinit var parent: MainView

    private val provincesList = flowpane {
        prefHeight = 148.toDouble()
    }

    val provinceToggleSubject: BehaviorSubject<Province> = BehaviorSubject.create()
    val windowResize: BehaviorSubject<Any> = BehaviorSubject.create()

    override val root = scrollpane {
        hbarPolicy = ScrollPane.ScrollBarPolicy.NEVER
        prefHeight = 200.toDouble()
        hgrow = Priority.ALWAYS
        vgrow = Priority.NEVER
        this@scrollpane.addChildIfPossible(provincesList)
    }

    init {
        GridPane.setColumnIndex(root, 0)
        GridPane.setRowIndex(root, 1)
    }

    fun showProvinces(provinces: List<Province>) {
        provincesList.children.clear()
        val toggleGroup = ToggleGroup()
        provinces.forEach {
            val radio = JFXRadioButton(it.name)
            radio.selectedColor = Color.BLUE
            radio.toggleGroup = toggleGroup
            radio.padding = Insets(10.0)
            radio.userData = it
            provincesList.children.add(radio)
        }
        toggleGroup.selectedToggleProperty().addListener({ _, _, new ->
            provinceToggleSubject.onNext(new.userData as Province?)
        })
    }

    fun alignProvinciesList() {
        provincesList.minWidthProperty().set(root.width)
    }

    fun inspectProvince(province: Province?) {
        parent.inspectProvince(province)
    }
}