package md.gg.ancientrivals.mapeditor.control_panel

import md.gg.ancientrivals.mapeditor.app.MainModel
import md.gg.ancientrivals.mapeditor.data.MapRepository
import md.gg.ancientrivals.mapeditor.data.domain.MapNode
import md.gg.ancientrivals.mapeditor.data.domain.Province
import tornadofx.*

class ControlModel : Component(), Injectable {
    private val mapRepository: MapRepository by inject()
    private val mainModel: MainModel by inject()

    val provinces: List<Province>
        get() = mapRepository.provinces

    val selectedNodes: MutableList<MapNode>
        get() = mainModel.selectedNodes

    fun saveProvinces() {
        mapRepository.save()
    }

    fun addProvince(province: Province) {
        mainModel.selectedProvince = province
        mapRepository.map.provinces.add(province)
    }
}