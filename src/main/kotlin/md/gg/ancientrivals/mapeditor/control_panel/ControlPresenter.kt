package md.gg.ancientrivals.mapeditor.control_panel

import md.gg.ancientrivals.mapeditor.data.domain.MapNode
import md.gg.ancientrivals.mapeditor.data.domain.Province
import md.gg.ancientrivals.mapeditor.province_inspector.ProvinceInspectorView
import md.gg.ancientrivals.mapeditor.province_list.ProvinceListView
import tornadofx.*

class ControlPresenter : Controller() {
    private val view: ControlView by inject()
    private val provinceListView: ProvinceListView by inject()
    private val provinceInspectorView: ProvinceInspectorView by inject()
    private val model: ControlModel by inject()

    fun init() {
        view.buttonSave.subscribe {
            model.saveProvinces()
        }

        view.buttonProvince.subscribe {
            val connections: MutableList<Pair<MapNode, MapNode>> = mutableListOf()
            for (i in 0..model.selectedNodes.size - 2) {
                connections.add(model.selectedNodes[i] to model.selectedNodes[i + 1])
            }
            connections.add(model.selectedNodes.last() to model.selectedNodes.first())
            val province = Province(name = "New Province", mapConnections = connections)
            model.addProvince(province)
            provinceListView.showProvinces(model.provinces)
            provinceInspectorView.inspectProvince(province)
            model.selectedNodes.clear()
        }
    }
}