package md.gg.ancientrivals.mapeditor.control_panel

import com.jfoenix.controls.JFXComboBox
import io.reactivex.subjects.BehaviorSubject
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.layout.Priority
import md.gg.ancientrivals.mapeditor.app.Styles
import md.gg.ancientrivals.mapeditor.ui.HoverMaterialButton
import tornadofx.*

class ControlView : View() {
    val buttonSave: BehaviorSubject<Any> = BehaviorSubject.create()
    val buttonProvince: BehaviorSubject<Unit> = BehaviorSubject.create()

    override val root = vbox {
        alignment = Pos.BOTTOM_RIGHT
        prefHeight = 154.toDouble()
        prefWidth = 256.toDouble()
        hgrow = Priority.ALWAYS
        maxWidth = 256.toDouble()
        style {
            padding = box(8.px)
        }

        this += HoverMaterialButton("Province").apply {
            action { buttonProvince.onNext(Unit) }
            addClass(Styles.materialButton)
        }

        region {
            prefHeight = 8.0
        }

        this += JFXComboBox<Label>().apply {
            promptText = "Editor Type"
            items.add(Label("Provinces"))
            items.add(Label("Terrain"))
            items.add(Label("Rivers"))
        }

        region {
            prefHeight = 8.0
        }

        this += HoverMaterialButton("Save".toUpperCase()).apply {
            action { buttonSave.onNext(Any()) }
            addClass(Styles.materialButton)
        }
    }
}