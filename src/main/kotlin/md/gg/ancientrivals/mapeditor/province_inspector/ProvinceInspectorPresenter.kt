package md.gg.ancientrivals.mapeditor.province_inspector

import md.gg.ancientrivals.mapeditor.province_list.ProvinceListView
import tornadofx.*

class ProvinceInspectorPresenter : Controller() {
    private val view: ProvinceInspectorView by inject()
    private val provinceListView: ProvinceListView by inject()
    private val model: ProvinceInspectorModel by inject()

    fun init() {
        subscribeToProvinceNameChanged()

        subscribeToProvinceCapitalSelect()
        subscribeToProvinceTypeSelect()

        subscribeToResourceRemove()
        subscribeToCityRemove()

        subscribeToAddResource()
        subscribeToAddCity()

        subscribeToAddCityDialogClosed()
        subscribeToAddResourceDialogClosed()
    }

    private fun subscribeToProvinceNameChanged() {
        view.provinceNameSubject.subscribe {
            model.selectedProvince?.name = it
            provinceListView.showProvinces(model.provinces)
        }
    }

    private fun subscribeToAddResourceDialogClosed() {
        view.addResourceDialogClosedSubject.subscribe {
            if (model.selectedProvince != null) {
                model.selectedProvince!!.resources.add(it)
                view.inspectProvince(model.selectedProvince!!)
            }
        }
    }

    private fun subscribeToAddCityDialogClosed() {
        view.addCityDialogClosedSubject.subscribe {
            if (model.selectedProvince != null) {
                model.selectedProvince!!.cities.add(it)
                view.inspectProvince(model.selectedProvince!!)
            }
        }
    }

    private fun subscribeToAddCity() {
        view.addCitySubject.subscribe {
            view.showAddCityDialog()
        }
    }

    private fun subscribeToAddResource() {
        view.addResourceSubject.subscribe {
            view.showAddResourceDialog()
        }
    }

    private fun subscribeToResourceRemove() {
        view.resourceRemoveSubject.subscribe {
            if (model.selectedProvince != null) {
                model.selectedProvince!!.resources.remove(it)
                view.inspectProvince(model.selectedProvince!!)
            }
        }
    }

    private fun subscribeToCityRemove() {
        view.cityRemoveSubject.subscribe {
            if (model.selectedProvince != null) {
                model.selectedProvince!!.cities.remove(it)
                view.inspectProvince(model.selectedProvince!!)
            }
        }
    }

    private fun subscribeToProvinceTypeSelect() {
        view.provinceTypeSelectedSubject.subscribe {
            if (model.selectedProvince != null) {
                model.selectedProvince!!.type = it
            }
        }
    }

    private fun subscribeToProvinceCapitalSelect() {
        view.provinceCapitalSelectedSubject.subscribe {
            if (model.selectedProvince != null) {
                model.selectedProvince!!.capital = it
            }
        }
    }
}