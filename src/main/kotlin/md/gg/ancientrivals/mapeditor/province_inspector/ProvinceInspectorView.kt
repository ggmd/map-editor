package md.gg.ancientrivals.mapeditor.province_inspector

import com.jfoenix.controls.JFXComboBox
import com.jfoenix.controls.JFXDialog
import com.jfoenix.controls.JFXListView
import com.jfoenix.controls.JFXTextField
import io.reactivex.subjects.BehaviorSubject
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.layout.Region
import md.gg.ancientrivals.mapeditor.app.MainView
import md.gg.ancientrivals.mapeditor.app.Styles
import md.gg.ancientrivals.mapeditor.data.domain.*
import md.gg.ancientrivals.mapeditor.ui.AddCityDialog
import md.gg.ancientrivals.mapeditor.ui.AddResourceDialog
import md.gg.ancientrivals.mapeditor.ui.HoverMaterialButton
import md.gg.ancientrivals.mapeditor.ui.RemovableLabel
import tornadofx.*

@Suppress("UNCHECKED_CAST")
class ProvinceInspectorView : View() {
    val provinceCapitalSelectedSubject: BehaviorSubject<Civilization> = BehaviorSubject.create()
    val provinceTypeSelectedSubject: BehaviorSubject<ProvinceType> = BehaviorSubject.create()

    val provinceNameSubject: BehaviorSubject<String> = BehaviorSubject.create()

    val addCitySubject: BehaviorSubject<Any> = BehaviorSubject.create()
    val addCityDialogClosedSubject: BehaviorSubject<City> = BehaviorSubject.create()

    val addResourceSubject: BehaviorSubject<Any> = BehaviorSubject.create()
    val addResourceDialogClosedSubject: BehaviorSubject<Resource> = BehaviorSubject.create()

    val resourceRemoveSubject: BehaviorSubject<Resource> = BehaviorSubject.create()
    var cityRemoveSubject: BehaviorSubject<City> = BehaviorSubject.create()

    lateinit var parent: MainView

    private val provinceName = JFXTextField().apply {
        promptText = "Province Name"
        textProperty().addListener { _, _, new -> provinceNameSubject.onNext(new) }
    }

    private val provinceCapitalOf = JFXComboBox<Label>().apply {
        promptText = "Capital Of"
        valueProperty().addListener({ _, _, new ->
            provinceCapitalSelectedSubject.onNext(new.userData as Civilization?)
        })
        hgrow = Priority.ALWAYS
        maxWidth = Double.MAX_VALUE
    }

    private val provinceType = JFXComboBox<Label>().apply {
        promptText = "Province Type"
        valueProperty().addListener({ _, _, new ->
            provinceTypeSelectedSubject.onNext(new.userData as ProvinceType?)
        })
        hgrow = Priority.ALWAYS
        maxWidth = Double.MAX_VALUE
    }

    private val citiesList = JFXListView<HBox>().apply {
        vgrow = Priority.ALWAYS
        minHeight = 128.toDouble()
    }

    private val resourceList = JFXListView<HBox>().apply {
        vgrow = Priority.ALWAYS
        minHeight = 128.toDouble()
    }

    override val root = vbox {
        alignment = Pos.TOP_CENTER
        maxHeight = Double.MAX_VALUE
        prefHeight = 504.toDouble()
        prefWidth = 256.toDouble()
        padding = Insets(8.toDouble())
        this += Region().apply { minHeight = 8.toDouble() }
        this += provinceName
        this += Region().apply { minHeight = 8.toDouble() }
        this += provinceCapitalOf
        this += Region().apply { minHeight = 8.toDouble() }
        this += provinceType
        this += Region().apply { minHeight = 8.toDouble() }
        this += citiesList
        this += Region().apply { minHeight = 8.toDouble() }
        hbox {
            alignment = Pos.CENTER_RIGHT
            this += HoverMaterialButton("Add City".toUpperCase()).apply {
                action { addCitySubject.onNext(Any()) }
                addClass(Styles.materialButton)

            }
            this += Region().apply { minWidth = 8.toDouble() }
        }
        this += Region().apply { minHeight = 8.toDouble() }
        this += resourceList
        this += Region().apply { minHeight = 8.toDouble() }
        hbox {
            alignment = Pos.CENTER_RIGHT
            this += HoverMaterialButton("Add Resource".toUpperCase()).apply {
                action { addResourceSubject.onNext(Any()) }
                addClass(Styles.materialButton)
            }
            this += Region().apply { minWidth = 8.toDouble() }
        }
        this += Region().apply { minHeight = 8.toDouble() }
    }

    init {
        Civilization.values().forEach {
            provinceCapitalOf.items.add(
                    Label(it.capital).apply {
                        userData = it
                    }
            )
        }
        ProvinceType.values().forEach {
            provinceType.items.add(
                    Label(it.type).apply {
                        userData = it
                    }
            )
        }
    }

    fun inspectProvince(province: Province) {
        provinceName.text = province.name
        provinceCapitalOf.selectionModel.select(Civilization.values().indexOf(province.capital))
        provinceType.selectionModel.select(ProvinceType.values().indexOf(province.type))

        citiesList.items.clear()
        province.cities.forEach { city ->
            val rLabel = RemovableLabel(city.name)
            rLabel.onDeleteButtonClick.subscribe { cityRemoveSubject.onNext(city) }
            citiesList.items.add(rLabel.view)
        }

        resourceList.items.clear()
        province.resources.forEach { resource ->
            val rLabel = RemovableLabel(resource.resourceName)
            rLabel.onDeleteButtonClick.subscribe { resourceRemoveSubject.onNext(resource) }
            resourceList.items.add(rLabel.view)
        }
    }

    fun showAddCityDialog() {
        val dialog = AddCityDialog(parent.uiRoot, JFXDialog.DialogTransition.RIGHT)
        dialog.closed.subscribe { addCityDialogClosedSubject.onNext(it) }
        dialog.show()
    }

    fun showAddResourceDialog() {
        val dialog = AddResourceDialog(parent.uiRoot, JFXDialog.DialogTransition.RIGHT)
        dialog.closed.subscribe { addResourceDialogClosedSubject.onNext(it) }
        dialog.show()
    }

    fun clear() {
        provinceName.clear()
        provinceCapitalOf.selectionModel.select(0)
        provinceType.selectionModel.select(0)
        citiesList.items.clear()
        resourceList.items.clear()
    }
}