package md.gg.ancientrivals.mapeditor.province_inspector

import md.gg.ancientrivals.mapeditor.app.MainModel
import md.gg.ancientrivals.mapeditor.data.MapRepository
import md.gg.ancientrivals.mapeditor.data.domain.Province
import tornadofx.*

class ProvinceInspectorModel : Component(), Injectable {
    private val mainModel: MainModel by inject()
    private val mapRepository: MapRepository by inject()

    val selectedProvince: Province?
        get() = mainModel.selectedProvince

    val provinces: List<Province>
        get() = mapRepository.provinces
}