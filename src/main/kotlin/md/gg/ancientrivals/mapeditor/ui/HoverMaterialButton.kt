package md.gg.ancientrivals.mapeditor.ui

import com.jfoenix.controls.JFXButton
import md.gg.ancientrivals.mapeditor.app.Styles
import tornadofx.addClass

class HoverMaterialButton(val theTextt: String = "") : JFXButton(theTextt.toUpperCase()) {
    init {
        setOnMouseEntered { this.buttonType = JFXButton.ButtonType.RAISED }
        setOnMouseExited { this.buttonType = JFXButton.ButtonType.FLAT }
        addClass(Styles.materialButton)
    }
}