package md.gg.ancientrivals.mapeditor.ui

import com.jfoenix.controls.JFXComboBox
import com.jfoenix.controls.JFXDialog
import com.jfoenix.controls.JFXDialogLayout
import io.reactivex.subjects.BehaviorSubject
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.layout.StackPane
import md.gg.ancientrivals.mapeditor.data.domain.Resource
import tornadofx.View
import tornadofx.action
import tornadofx.hbox
import tornadofx.paddingTop
import tornadofx.plusAssign
import tornadofx.selectedItem
import tornadofx.vbox

class AddResourceDialog(val parent: StackPane, val transition: JFXDialog.DialogTransition) : View() {
    val closed: BehaviorSubject<Resource> = BehaviorSubject.create()

    private val resource = JFXComboBox<Label>().apply {
        promptText = "Select Resource"
    }
    private val add = HoverMaterialButton("Add").apply {
        action {
            closed.onNext(resource.selectedItem!!.userData as Resource)
            dialog.close()
        }
    }
    override val root = vbox {
        this += resource
        hbox {
            alignment = Pos.BOTTOM_RIGHT
            paddingTop = 16.0
            this += add
        }
    }

    private val content: JFXDialogLayout = JFXDialogLayout().apply { children.add(root) }
    private val dialog = JFXDialog(parent, content, transition)

    init {
        Resource.values().forEach { resource.items.add(Label(it.resourceName).apply { userData = it }) }
    }

    fun show() {
        dialog.show()
    }
}