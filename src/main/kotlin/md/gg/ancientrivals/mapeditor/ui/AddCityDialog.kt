package md.gg.ancientrivals.mapeditor.ui

import com.jfoenix.controls.JFXDialog
import com.jfoenix.controls.JFXDialogLayout
import com.jfoenix.controls.JFXTextField
import io.reactivex.subjects.BehaviorSubject
import javafx.geometry.Pos
import javafx.scene.layout.StackPane
import md.gg.ancientrivals.mapeditor.data.domain.City
import tornadofx.View
import tornadofx.action
import tornadofx.hbox
import tornadofx.paddingTop
import tornadofx.plusAssign
import tornadofx.vbox

class AddCityDialog(val parent: StackPane, val transition: JFXDialog.DialogTransition) : View() {
    val closed: BehaviorSubject<City> = BehaviorSubject.create()

    private val cityName = JFXTextField().apply { promptText = "City Name" }
    private val add = HoverMaterialButton("Add").apply {
        action {
            closed.onNext(City(cityName.text))
            dialog.close()
        }
    }
    override val root = vbox {
        this += cityName
        hbox {
            alignment = Pos.BOTTOM_RIGHT
            paddingTop = 16.0
            this@hbox += add
        }
    }
    private val content: JFXDialogLayout = JFXDialogLayout().apply { children.add(root) }
    private val dialog = JFXDialog(parent, content, transition)

    fun show() {
        dialog.show()
    }
}