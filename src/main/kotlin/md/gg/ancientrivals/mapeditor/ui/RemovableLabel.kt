package md.gg.ancientrivals.mapeditor.ui

import com.jfoenix.controls.JFXButton
import de.jensd.fx.glyphs.GlyphsDude
import de.jensd.fx.glyphs.materialicons.MaterialIcon
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.layout.Region
import javafx.scene.text.Text
import tornadofx.action
import tornadofx.hgrow

class RemovableLabel {
    val view: HBox = HBox()
    var text: String = ""
        set(value) {
            field = value
            label.text = value
        }
    val onDeleteButtonClick: Subject<Any> = BehaviorSubject.create()

    private val label: Label = Label()
    private val icon: Text = GlyphsDude.createIcon(MaterialIcon.DELETE)
    private val button: JFXButton = JFXButton()

    constructor(text: String) {
        this.text = text
    }

    init {
        button.graphic = icon

        view.alignment = Pos.CENTER_LEFT
        view.children.add(label)
        val region = Region()
        region.hgrow = Priority.ALWAYS
        view.children.add(region)
        view.children.add(button)
        view.hgrow = Priority.ALWAYS

        button.apply {
            action { onDeleteButtonClick.onNext(Any()) }
            setOnMouseEntered { buttonType = JFXButton.ButtonType.RAISED }
            setOnMouseExited { buttonType = JFXButton.ButtonType.FLAT }
        }
    }
}