package md.gg.ancientrivals.mapeditor.app

import javafx.stage.Stage
import md.gg.ancientrivals.mapeditor.control_panel.ControlPresenter
import md.gg.ancientrivals.mapeditor.editor.EditorPresenter
import md.gg.ancientrivals.mapeditor.province_inspector.ProvinceInspectorPresenter
import md.gg.ancientrivals.mapeditor.province_list.ProvinceListPresenter
import tornadofx.App
import tornadofx.reloadStylesheetsOnFocus

class MapEditorApp : App(MainView::class, Styles::class) {
    val mainPresenter: MainPresenter by inject()
    val controlPresenter: ControlPresenter by inject()
    val provinceInspectorPresenter: ProvinceInspectorPresenter by inject()
    val provinceListPresenter: ProvinceListPresenter by inject()
    val editorPresenter: EditorPresenter by inject()

    override fun start(stage: Stage) {
        super.start(stage)
        mainPresenter.init()
        controlPresenter.init()
        provinceInspectorPresenter.init()
        provinceListPresenter.init()
        editorPresenter.init()
    }

    init {
        reloadStylesheetsOnFocus()
    }
}