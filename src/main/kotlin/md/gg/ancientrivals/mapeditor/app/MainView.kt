package md.gg.ancientrivals.mapeditor.app

import com.jfoenix.controls.JFXDecorator
import javafx.scene.layout.ColumnConstraints
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import javafx.scene.layout.RowConstraints
import javafx.scene.layout.StackPane
import md.gg.ancientrivals.mapeditor.control_panel.ControlView
import md.gg.ancientrivals.mapeditor.data.domain.Province
import md.gg.ancientrivals.mapeditor.editor.EditorView
import md.gg.ancientrivals.mapeditor.province_inspector.ProvinceInspectorView
import md.gg.ancientrivals.mapeditor.province_list.ProvinceListView
import tornadofx.View
import tornadofx.plusAssign

class MainView : View() {
    private val controlView: ControlView by inject()
    private val provinceInspectorView: ProvinceInspectorView by inject()
    private val provinceListView: ProvinceListView by inject()
    private val editorView: EditorView by inject()
    private val gridPane = GridPane().apply {
        maxHeight = Double.MAX_VALUE
        maxWidth = Double.MAX_VALUE
        columnConstraints.add(ColumnConstraints().apply {
            hgrow = Priority.ALWAYS
            maxWidth = Double.MAX_VALUE
            prefWidth = 914.toDouble()
        })
        columnConstraints.add(ColumnConstraints().apply {
            fillWidthProperty().set(false)
            hgrow = Priority.NEVER
            maxWidth = 256.toDouble()
            prefWidth = 256.toDouble()
        })
        rowConstraints.add(RowConstraints().apply {
            maxHeight = Double.MAX_VALUE
            prefHeight = 504.toDouble()
            vgrow = Priority.ALWAYS
        })
        rowConstraints.add(RowConstraints().apply {
            fillHeightProperty().set(false)
            maxHeight = 205.toDouble()
            prefHeight = 154.toDouble()
            vgrow = Priority.NEVER
        })
        children.add(controlView.root)
        children.add(provinceInspectorView.root)
    }
    val uiRoot = StackPane().apply {
        maxHeight = Double.MAX_VALUE
        maxWidth = Double.MAX_VALUE
        prefWidth = 1024.toDouble()
        prefHeight = 654.toDouble()
        this += gridPane
    }
    override val root = JFXDecorator(primaryStage, uiRoot)


    init {
        listenToWindowResize()

        GridPane.setColumnIndex(controlView.root, 1)
        GridPane.setRowIndex(controlView.root, 1)
        GridPane.setColumnIndex(provinceInspectorView.root, 1)
        gridPane.children.add(provinceListView.root)
        gridPane.children.add(editorView.root)

        provinceInspectorView.parent = this
        provinceListView.parent = this
    }

    private fun listenToWindowResize() {
        primaryStage.widthProperty().addListener({
            _, _, _ ->
            provinceListView.alignProvinciesList()
        })
    }

    fun inspectProvince(province: Province?) {
        provinceInspectorView.inspectProvince(province!!)
    }
}