package md.gg.ancientrivals.mapeditor.app

import md.gg.ancientrivals.mapeditor.data.domain.MapNode
import md.gg.ancientrivals.mapeditor.data.domain.Province
import tornadofx.*

class MainModel : Component(), Injectable {
    var selectedProvince: Province? = null
    val selectedNodes: MutableList<MapNode> = mutableListOf()
}