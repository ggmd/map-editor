package md.gg.ancientrivals.mapeditor.app

import tornadofx.Controller

class MainPresenter : Controller() {
    val view: MainView by inject()
    val model: MainModel by inject()

    fun init() {

    }
}