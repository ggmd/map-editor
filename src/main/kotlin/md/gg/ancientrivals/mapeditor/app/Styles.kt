package md.gg.ancientrivals.mapeditor.app

import javafx.scene.text.FontWeight
import tornadofx.*

class Styles : Stylesheet() {
    companion object {
        val container by cssclass()
        val uiComponent by cssclass()
        val materialButton by cssclass()
    }

    init {
        uiComponent {
            startMargin = 8.px
            endMargin = 8.px
            gapStartAndEnd = true
        }
        materialButton {
            backgroundColor += c("#283593")
            textFill = c("#FFFFFFFF")
            fontWeight = FontWeight.BOLD
        }
        container {
            padding = box(8.px)
        }
    }
}